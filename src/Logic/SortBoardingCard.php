<?php

namespace App\Logic;

use App\Exception\InvalidLocations;
use App\Model\BoardingCard;
use Doctrine\Common\Collections\ArrayCollection;

class SortBoardingCard
{
    /**
     * This method is used to sort an unsorted boarding cards array, we use only the starting and endind point meaning
     * that it is difficult to sort an array with travel loop. Adding time of the travel could definitly help in this case.
     * So in case of loop, an InvalidLocations error is thrown for now.
     *
     * @param array<BoardingCard> $boardingCards: an unsorted array of boarding cards
     * 
     * @return array<BoardingCard> The sorted array of boarding cards
     */
    public static function sortBoardingCard(array $boardingCards): array
    {
        // Search the starting point
        $unsortedBoardingCards = new ArrayCollection($boardingCards);
        $sortedBoardingCards = new ArrayCollection([]);
        $startingBdCard = self::getFirstLocation($unsortedBoardingCards);
        $sortedBoardingCards->add($startingBdCard);

        $currentBdCard = $startingBdCard;
        while ($unsortedBoardingCards->count() !== $sortedBoardingCards->count()) {
            $nextBdCard = self::getSuccessor($currentBdCard, $unsortedBoardingCards);
            $sortedBoardingCards->add($nextBdCard);
            $currentBdCard = $nextBdCard;
        }

        return $sortedBoardingCards->toArray();
    }

    /**
     * Find the first boarding card (first step of the journey) in an unsorted array.
     *
     * @param ArrayCollection<int, BoardingCard> $unsortedBoardingCards: the other boarding card
     *
     * @return BoardingCard The first boarding card
     */
    private static function getFirstLocation(ArrayCollection $unsortedBoardingCards): BoardingCard
    {
        $startingBdCard = null;
        foreach ($unsortedBoardingCards as $bdCard) {
            $isStartPoint = $unsortedBoardingCards->forAll(
                function($key, $otherBdCard) use ($bdCard) {
                    return $bdCard->getStartPoint() !== $otherBdCard->getEndPoint();
                }
            );
            if ($isStartPoint) {
                if (null === $startingBdCard) {
                    $startingBdCard = $bdCard;
                } else {
                    throw new InvalidLocations('There is more than one location with no predecessor!');
                }
            }
        }
        if (null === $startingBdCard) {
            throw new InvalidLocations('All location have a predecessor!');
        }

        return $startingBdCard;
    }

    /**
     * Find the succesor of a boarding card in an unsorted array of boarding card.
     *
     * @param ArrayCollection<int, BoardingCard> $unsortedBoardingCards: the unsorted array of boarding card
     *
     * @return BoardingCard The next boarding card
     */
    private static function getSuccessor(BoardingCard $currentBdCard, ArrayCollection $unsortedBoardingCards): BoardingCard
    {
        $potentialSuccessor = $unsortedBoardingCards->filter(function ($bdCard) use ($currentBdCard) {
            return $bdCard->getStartPoint() === $currentBdCard->getEndPoint();
        });
        if (1 === $potentialSuccessor->count()) {
            return $potentialSuccessor->first();
        }
        if (0 === $potentialSuccessor->count()) {
            throw new InvalidLocations('No succesor found!');
        }
        throw new InvalidLocations('To many successor found!');
    }
}
