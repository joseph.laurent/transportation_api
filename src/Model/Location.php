<?php

namespace App\Model;

abstract class Location
{
    public const PARIS = 'Paris';
    public const LYON = 'Lyon';
    public const BERLIN = 'Berlin';
    public const NEW_YORK = 'New York';
    public const MONTREAL = 'Montreal';
    public const TOKYO = 'Tokyo';
    public const MOSCOU = 'Moscou';
}
