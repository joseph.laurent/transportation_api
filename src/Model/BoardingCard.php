<?php

namespace App\Model;

use App\Exception\InvalidStartAndEndPoint;
use App\Exception\InvalidTransportationType;

class BoardingCard
{
    protected string $startPoint;
    protected string $endPoint;
    protected ?string $seat;

    public function __construct(string $startPoint, string $endPoint, ?string $seat)
    {
        if ($startPoint === $endPoint) {
            throw new InvalidStartAndEndPoint();
        }
        $this->startPoint = $startPoint;
        $this->endPoint = $endPoint;
        $this->seat = $seat;
    }

    /**
     * This method is used to compare 2 boarding cards.
     *
     * @return bool TRUE if and only if the 2 boarding cards are equals
     */
    public function isEquals(self $otherBoardingCard): bool
    {
        return $this->startPoint === $otherBoardingCard->startPoint &&
               $this->endPoint === $otherBoardingCard->endPoint;
    }

    /**
     * Create various boarding cards depending on the transportation type.
     * the date to differentiate 2 boarding cards with the same starting/ending point.
     *
     * @return BoardingCard The created boarding card
     */
    public static function create(string $transportationType, string $startPoint, string $endPoint,
                           ?string $seat = null, ?int $transportationNumber = null, ?string $gate = null): self
    {
        if (TransportationType::UNKNOW === $transportationType) {
            return new self($startPoint, $endPoint, $seat);
        } elseif (TransportationType::FLIGHT === $transportationType) {
            return new FlightBoardingCard($startPoint, $endPoint, $seat, $transportationNumber, $gate);
        } elseif (TransportationType::BUS === $transportationType) {
            return new BusBoardingCard($startPoint, $endPoint, $seat, $transportationNumber);
        }
        throw new InvalidTransportationType('Invalid transportation type given ('.$transportationType.')!');
    }

    public function getStartPoint(): string
    {
        return $this->startPoint;
    }

    public function getEndPoint(): string
    {
        return $this->endPoint;
    }
}
