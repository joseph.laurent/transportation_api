<?php

namespace App\Model;

class BusBoardingCard extends BoardingCard
{
    protected ?int $busNumber;
    protected ?string $gate;

    public function __construct(string $startPoint, string $endPoint, ?string $seat = null, ?int $busNumber = null)
    {
        parent::__construct($startPoint, $endPoint, $seat);
        $this->busNumber = $busNumber;
    }

    public function getBusNumber(): ?int
    {
        return $this->busNumber;
    }
}
