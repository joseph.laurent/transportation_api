<?php

namespace App\Model;

class FlightBoardingCard extends BoardingCard
{
    protected ?int $flightNumber;
    protected ?string $gate;

    public function __construct(string $startPoint, string $endPoint, ?string $seat, ?int $flightNumber, ?string $gate)
    {
        parent::__construct($startPoint, $endPoint, $seat);
        $this->flightNumber = $flightNumber;
        $this->gate = $gate;
    }

    public function getGate(): ?string
    {
        return $this->gate;
    }
}
