<?php

namespace App\Model;

abstract class TransportationType
{
    public const UNKNOW = 'Unknow';
    public const BUS = 'Bus';
    public const FLIGHT = 'Flight';
    public const TRAIN = 'Train';
}
