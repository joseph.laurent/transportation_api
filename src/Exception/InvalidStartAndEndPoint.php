<?php

namespace App\Exception;

use Exception;

class InvalidStartAndEndPoint extends Exception
{
    public function __construct($message = 'Start point and End poit must be the same!', $code = 0)
    {
        parent::__construct($message, $code);
    }

    public function __toString()
    {
        return $this->message;
    }
}
