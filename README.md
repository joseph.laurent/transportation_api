# transportation_api

Api to sort array of boarding cards.

# To use php-cs-fixer

```
vendor/bin/php-cs-fixer fix
```

# To use PHPStan

```
vendor/bin/phpstan analyse src --level 7
```

# To run the unit test
```
vendor/bin/phpunit  --verbose --coverage-text tests/
```
