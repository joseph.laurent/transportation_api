<?php

use App\Exception\InvalidLocations;
use App\Exception\InvalidStartAndEndPoint;
use App\Exception\InvalidTransportationType;
use App\Logic\SortBoardingCard;
use App\Model\BoardingCard;
use App\Model\Location;
use App\Model\TransportationType;
use PHPUnit\Framework\TestCase;

class SortBoardingCardTest extends TestCase
{
    public function testBoardingCardDataModel(): void
    {
        $gate = '7B';
        $boardingCard = BoardingCard::create(TransportationType::FLIGHT, Location::PARIS,
                                              Location::MONTREAL, '17A', 83, $gate);
        $this->assertSame($boardingCard->getGate(), $gate);
    }

    public function testWrongTransportationType(): void
    {
        $this->expectException(InvalidTransportationType::class);
        BoardingCard::create('Bsu', Location::PARIS, Location::MONTREAL);
    }

    public function testWrongLocationBoardingCard(): void
    {
        $this->expectException(InvalidStartAndEndPoint::class);
        BoardingCard::create(TransportationType::BUS, Location::PARIS, Location::PARIS);
    }

    public function testSortBoardingCardSimple(): void
    {
        $firstBoardingCard = BoardingCard::create(TransportationType::BUS, Location::LYON, Location::PARIS);
        $secondBoardingCard = BoardingCard::create(TransportationType::BUS, Location::PARIS, Location::MOSCOU);
        $unsortedBoardingCards = [
            $secondBoardingCard,
            $firstBoardingCard,
        ];
        $sortedList = SortBoardingCard::sortBoardingCard($unsortedBoardingCards);
        $this->assertSame(count($sortedList), 2);
        $this->assertTrue($sortedList[0] === $firstBoardingCard);
        $this->assertTrue($sortedList[1] === $secondBoardingCard);
    }

    public function testSortBoardingCard(): void
    {
        // More complex test with 6 boarding cards
        $unsortedBoardingCards = [
            BoardingCard::create(TransportationType::BUS, Location::LYON, Location::PARIS),
            BoardingCard::create(TransportationType::FLIGHT, Location::PARIS, Location::MOSCOU),
            BoardingCard::create(TransportationType::FLIGHT, Location::MOSCOU, Location::MONTREAL),
            BoardingCard::create(TransportationType::UNKNOW, Location::MONTREAL, Location::NEW_YORK),
            BoardingCard::create(TransportationType::FLIGHT, Location::NEW_YORK, Location::TOKYO),
            BoardingCard::create(TransportationType::FLIGHT, Location::TOKYO, Location::BERLIN),
        ];
        shuffle($unsortedBoardingCards);
        $sortedList = SortBoardingCard::sortBoardingCard($unsortedBoardingCards);
        $this->assertSame(count($sortedList), 6);
        for ($index = 1; $index < count($sortedList); ++$index) {
            $this->assertTrue($sortedList[$index - 1]->getEndPoint() === $sortedList[$index]->getStartPoint());
        }
    }

    public function testSortBoardingCardFailureSimple(): void
    {
        // Location are not consistent
        $firstBoardingCard = BoardingCard::create(TransportationType::BUS, Location::LYON, Location::PARIS);
        $secondBoardingCard = BoardingCard::create(TransportationType::BUS, Location::MOSCOU, Location::PARIS);
        $unsortedBoardingCards = [
            $secondBoardingCard,
            $firstBoardingCard,
        ];
        $this->expectException(InvalidLocations::class);
        SortBoardingCard::sortBoardingCard($unsortedBoardingCards);
    }

    public function testSortBoardingCardFailure(): void
    {
        // Location are not consistent
        $unsortedBoardingCards = [
            BoardingCard::create(TransportationType::BUS, Location::LYON, Location::PARIS),
            BoardingCard::create(TransportationType::FLIGHT, Location::PARIS, Location::MOSCOU),
            BoardingCard::create(TransportationType::FLIGHT, Location::MOSCOU, Location::PARIS), // The error (PARIS instead of MONTREAL)
            BoardingCard::create(TransportationType::UNKNOW, Location::MONTREAL, Location::NEW_YORK),
            BoardingCard::create(TransportationType::FLIGHT, Location::NEW_YORK, Location::TOKYO),
            BoardingCard::create(TransportationType::FLIGHT, Location::TOKYO, Location::BERLIN),
        ];
        shuffle($unsortedBoardingCards);
        $this->expectException(InvalidLocations::class);
        SortBoardingCard::sortBoardingCard($unsortedBoardingCards);
    }

    public function testSortBoardingCardFailureLoop(): void
    {
        // Location define a loop (impossible to find the starting point of the journey)
        $unsortedBoardingCards = [
            BoardingCard::create(TransportationType::BUS, Location::LYON, Location::PARIS),
            BoardingCard::create(TransportationType::FLIGHT, Location::PARIS, Location::MOSCOU),
            BoardingCard::create(TransportationType::FLIGHT, Location::MOSCOU, Location::LYON),
        ];
        shuffle($unsortedBoardingCards);
        $this->expectException(InvalidLocations::class);
        SortBoardingCard::sortBoardingCard($unsortedBoardingCards);
    }
}
